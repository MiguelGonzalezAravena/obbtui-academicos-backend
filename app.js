'use strict';

var config = require('./config'),
  express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE');
  res.setHeader('Access-Control-Max-Age', '60');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

var accesoRouter = require('./proyecto/routes/acceso.router'),
  asignaturasRouter = require('./proyecto/routes/asignaturas.router'),
  casinosRouter = require('./proyecto/routes/casinos.router'),
  enlacesRouter = require('./proyecto/routes/enlaces.router'),
  estudianteRouter = require('./proyecto/routes/estudiante.router'),
  eventosRouter = require('./proyecto/routes/eventos.router'),
  horarioRouter = require('./proyecto/routes/horario.router'),
  novedadesRouter = require('./proyecto/routes/novedades.router'),
  notificacionesRouter = require('./proyecto/routes/notificaciones.router'),
  puntosRouter = require('./proyecto/routes/puntos.router')
;

app.use('/apptui-academicos/acceso/', accesoRouter);
app.use('/apptui-academicos/asignaturas/', asignaturasRouter);
app.use('/apptui-academicos/casinos/', casinosRouter);
app.use('/apptui-academicos/enlaces/', enlacesRouter);
app.use('/apptui-academicos/estudiante/', estudianteRouter);
app.use('/apptui-academicos/eventos/', eventosRouter);
app.use('/apptui-academicos/horario/', horarioRouter);
app.use('/apptui-academicos/novedades/', novedadesRouter);
app.use('/apptui-academicos/notificaciones/', notificacionesRouter);
app.use('/apptui-academicos/puntos/', puntosRouter);

module.exports = app;