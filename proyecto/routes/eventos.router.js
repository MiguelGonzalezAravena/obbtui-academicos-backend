'use strict';
var express = require('express'),
  eventosService = require('../services/eventos.service'),
  router = express.Router();

router.post('/', eventosService.getEventos);

module.exports = router;