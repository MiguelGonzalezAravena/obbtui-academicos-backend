'use strict';
var express = require('express'),
  puntosService = require('../services/puntos.service'),
  router = express.Router();

router.post('/', puntosService.getPuntos);

module.exports = router;