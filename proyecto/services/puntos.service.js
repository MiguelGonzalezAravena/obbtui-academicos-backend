'use strict';
var reply = require('../../base/utils/reply');

function getPuntos(request, response) {
  try {
    // Obtener la lista de puntos de interés
    let puntos = [
      {
        nombre: 'DAE',
        direccion: 'CALLE FALSA #123, VALPARAÍSO',
        imagen: 'https://apple5x1.com/app/uploads/2017/04/Apple-Store-New-York-City-2000x1333-wide-wallpapers.net_.jpg',
        latitud: '40.7528995',
        longitud: '-73.977514'
      }
    ];

    response.json(reply.ok(puntos));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  getPuntos
};