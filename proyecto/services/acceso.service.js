'use strict';
var reply = require('../../base/utils/reply');

function acceso(request, response) {
  try {
    var args = JSON.parse(request.body.arg === undefined ? '{}' : request.body.arg),
      propRUN = args.hasOwnProperty('run') && args.run != null && args.run.length > 0,
      propContrasena = args.hasOwnProperty('contrasena') && args.contrasena != null && args.contrasena.length > 0;

    if(!propRUN) {
      response.json(reply.error('Parámetro "run" no encontrado.'));
    } else if(!propContrasena) {
      response.json(reply.error('Parámetro "contrasena" no encontrado.'));
    } else {
      /* Petición para ir a buscar recursos de usuario */
      if(args.run == '11111111' && args.contrasena == '123456') {
        let resp = {
          run: args.run,
          nombres: 'Juan',
          apellidos: 'Pérez',
          email: 'juan.perez@uv.cl',
          codCarrera: '10000',
          carrera: 'ING. CIVIL INFORMÁTICA',
          descEstado: 'Alumno Regular',
          fotografia: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678099-profile-filled-512.png',
          movilidad: false,
          token: '1234567890'
        };

        response.json(reply.ok(resp));
      } else if(args.run == '11111111' && args.contrasena != '123456') {
        response.json(reply.error('Usuario o Contraseña no son correctos.'));
      } else {
        response.json(reply.error('Usuario no encontrado.'));
      }
    }
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  acceso
};