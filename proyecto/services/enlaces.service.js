'use strict';
var reply = require('../../base/utils/reply');

function getEnlaces(request, response) {
  try {
    // Obtener la lista de enlaces
    let enlaces = [
      {
        nombre: 'DAE',
        enlace: 'http://dae.uv.cl'
      },
      {
        nombre: 'OBBTUI',
        enlace: 'http://observatoriotui.uv.cl'
      },
      {
        nombre: 'DTIC',
        enlace: 'http://dtic.uv.cl'
      }
    ];

    response.json(reply.ok(enlaces));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  getEnlaces
};