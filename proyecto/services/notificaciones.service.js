'use strict';
var reply = require('../../base/utils/reply');

function getNotificaciones(request, response) {
  try {
    // Obtener las notificaciones
    let resp = [
      {
        id: 1,
        titulo: 'Pruebas parciales',
        cuerpo: 'Tu nota se encuentra publicada.',
        fecha: formatoFecha('2018-03-27T18:00:00.000Z')
      },
      {
        id: 2,
        titulo: 'Clases suspendidas',
        cuerpo: 'El profesor de ESTRUCTURAS DISCRETAS ha suspendido la clase.',
        fecha: formatoFecha('2018-03-27T19:00:00.000Z')
      },
      {
        id: 3,
        titulo: 'Recordatorio',
        cuerpo: 'Recordar traer equipos para el próximo taller de OBBTUI.',
        fecha: formatoFecha('2018-03-29T10:00:00.000Z')
      }
    ];

    response.json(reply.ok(resp));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function getTipos(request, response) {
  try {
    // Obtener los tipos de notificaciones
    let tipos = [
      {
        id: 1,
        tipo: 'informacion',
        nombre: 'Información'
      },
      {
        id: 2,
        tipo: 'suspension',
        nombre: 'Suspensión'
      },
      {
        id: 3,
        tipo: 'enlace',
        nombre: 'Enlace'
      }
    ];

    response.json(reply.ok(tipos));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function sendNotificacion(request, response) {
  try {
    // Enviar notificación a los estudiantes
    var args = JSON.parse(request.body.arg === undefined ? '{}' : request.body.arg),
      propTipo = args.hasOwnProperty('tipo'),
      propMensaje = args.hasOwnProperty('mensaje'),
      propEstudiantes = args.hasOwnProperty('estudiantes');

    if(!propTipo) {
      response.json(reply.error('Parámetro "tipo" no encontrado'));
    } else if(!propMensaje) {
      response.json(reply.error('Parámetro "mensaje" no encontrado'));
    } else if(!propEstudiantes) {
      response.json(reply.error('Parámetro "estudiantes" no encontrado'));
    } else {
      // Enviar notificación a OneSignal
      freeze(4);
      response.json(reply.ok('Notificación enviada satisfactoriamente'));
    }
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function freeze(time) {
  const stop = new Date().getTime() + time * 1000;
  while(new Date().getTime() < stop);
}

function shortMonth(month) {
  switch(month) {
    case 0:
      return 'ENE';
      break;
    case 1:
      return 'FEB';
      break;
    case 2:
      return 'MAR';
      break;
    case 3:
      return 'ABR';
      break;
    case 4:
      return 'MAY';
      break;
    case 5:
      return 'JUN';
      break;
    case 6:
      return 'JUL';
      break;
    case 7:
      return 'AGO';
      break;
    case 8:
      return 'SEP';
      break;
    case 9:
      return 'OCT';
      break;
    case 10:
      return 'NOV';
      break;
    case 11:
      return 'DIC';
      break;
  }
}

function formatoFecha(date) {
  let fechaMensaje = new Date(date);
  let fecha = new Date();
  let diaHoy = fecha.getUTCDate();
  let mesHoy = fecha.getUTCMonth();
  let anioHoy = fecha.getUTCFullYear();
  let diaMensaje = fechaMensaje.getUTCDate();
  let mesMensaje = fechaMensaje.getUTCMonth();
  let anioMensaje = fechaMensaje.getUTCFullYear();
  let horaMensaje = strDate(fechaMensaje.getUTCHours()) + ':' + strDate(fechaMensaje.getUTCMinutes());

  if(diaHoy == diaMensaje && mesHoy == mesMensaje && anioHoy == anioMensaje) {
    return 'Hoy, a las ' + horaMensaje;
  } else if(diaMensaje == diaHoy - 1 && mesHoy == mesMensaje && anioHoy == anioMensaje) {
    return 'Ayer, a las ' + horaMensaje;
  } else if(anioHoy == anioMensaje) {
    return strDate(diaMensaje) + ' ' + shortMonth(mesMensaje) + ', a las ' + horaMensaje;
  } else {
    return strDate(diaMensaje) + ' ' + shortMonth(mesMensaje) + ' ' + anioMensaje + ', a las ' + horaMensaje;
  }
}

function strDate(num) {
  let result = ('0' + num).toString();
  return result.substr(-2);
}

module.exports = {
  getNotificaciones,
  getTipos,
  sendNotificacion
};