'use strict';
var reply = require('../../base/utils/reply');

function getHorario(request, response) {
  try {
    // Obtener el horario
    let horario = {
      Lunes: [
        [{ nomAsig: 'CÁLCULO I' }], // Es un arreglo porque en el bloque 0 (08:30 - 10:00) puede existir tope de horarios
        [{ nomAsig: 'CÁLCULO I' }],
        [], [], [], [], [], [], []
      ],
      Martes: [
        [], [], [], [], [], [], [], [], []
      ],
      Miercoles: [
        [{ nomAsig: 'CÁLCULO I' }],
        [{ nomAsig: 'CÁLCULO I' }],
        [], [], [], [], [], [], []
      ],
      Jueves: [
        [], [], [], [], [], [], [], [], []
      ],
      Viernes: [
        [{ nomAsig: 'CÁLCULO I' }],
        [{ nomAsig: 'CÁLCULO I' }],
        [], [], [], [], [], [], []
      ],
      Sabado: [
        [], [], [], [], [], [], [], [], []
      ]
    };

    response.json(reply.ok(horario));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function getBloques(request, response) {
  try {
    // Obtener los bloques de horario
    let bloques = [
      { horario: '08:30 - 10:00' },
      { horario: '10:15 - 11:45' },
      { horario: '12:00 - 13:30' },
      { horario: '14:30 - 15:00' },
      { horario: '15:15 - 16:45' },
      { horario: '17:00 - 18:30' },
      { horario: '18:45 - 20:15' },
      { horario: '20:30 - 22:00' },
      { horario: '22:15 - 23:45' }
    ];

    response.json(reply.ok(bloques));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  getHorario,
  getBloques
};