# Changelog
A continuación se listan los cambios realizados hasta la fecha.

## 1.0.3 (15 Marzo 2018)
- Arreglar error de bloques corridos en un bloque.
- Arreglar llamada asincrónica para fotografía encriptada al iniciar sesión.
- Arreglar cálculo de Año y Semestre máximo actual.

## 1.0.0 (09 Marzo 2018)
- Primera versión del controlador de la aplicación móvil AppTUI UV.